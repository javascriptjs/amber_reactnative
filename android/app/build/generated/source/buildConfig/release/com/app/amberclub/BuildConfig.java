/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.app.amberclub;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.app.amberclub";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 14;
  public static final String VERSION_NAME = "1.1.1";
}
